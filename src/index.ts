import xs, {Stream} from 'xstream';
import AsyncStorage from '@react-native-async-storage/async-storage';

export interface ClearCommand {
  type: 'clear';
}

export interface SetItemCommand {
  type: 'setItem';
  key: string;
  value: string;
}

export interface MergeItemCommand {
  type: 'mergeItem';
  key: string;
  value: string;
}

export interface RemoveItemCommand {
  type: 'removeItem';
  key: string;
}

export interface MultiSetCommand {
  type: 'multiSet';
  keyValuePairs: Array<[string, string]>;
}

export interface MultiMergeCommand {
  type: 'multiMerge';
  keyValuePairs: Array<[string, string]>;
}

export interface MultiRemoveCommand {
  type: 'multiRemove';
  keys: Array<string>;
}

export type Command =
  | ClearCommand
  | SetItemCommand
  | MergeItemCommand
  | RemoveItemCommand
  | MultiSetCommand
  | MultiMergeCommand
  | MultiRemoveCommand;

export class AsyncStorageSource {
  constructor() {}

  public getAllKeys() {
    return xs.fromPromise(AsyncStorage.getAllKeys());
  }

  public getItem(key: string) {
    return xs.fromPromise(AsyncStorage.getItem(key));
  }

  public multiGet(keys: Array<string>) {
    return xs.fromPromise(AsyncStorage.multiGet(keys));
  }
}

export function asyncStorageDriver(
  command$: Stream<Command>,
): AsyncStorageSource {
  command$.subscribe({
    next: cmd => {
      if (cmd.type === 'clear') {
        AsyncStorage.clear();
      } else if (cmd.type === 'setItem') {
        AsyncStorage.setItem(cmd.key, cmd.value);
      } else if (cmd.type === 'mergeItem') {
        AsyncStorage.mergeItem(cmd.key, cmd.value);
      } else if (cmd.type === 'removeItem') {
        AsyncStorage.removeItem(cmd.key);
      } else if (cmd.type === 'multiSet') {
        AsyncStorage.multiSet(cmd.keyValuePairs);
      } else if (cmd.type === 'multiMerge') {
        AsyncStorage.multiMerge(cmd.keyValuePairs);
      } else if (cmd.type === 'multiRemove') {
        AsyncStorage.multiRemove(cmd.keys);
      }
    },
  });

  return new AsyncStorageSource();
}
