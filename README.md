# Cycle Native AsyncStorage

**A Cycle.js Driver for using the [AsyncStorage](https://facebook.github.io/react-native/docs/0.55/asyncstorage) from React Native**

```
npm install cycle-native-asyncstorage
```

## Usage

### Sink

Stream of command objects, either one of these:

```ts
{
  type: 'clear';
}
```

```ts
{
  type: 'setItem';
  key: string;
  value: string;
}
```

```ts
{
  type: 'mergeItem';
  key: string;
  value: string;
}
```

```ts
{
  type: 'removeItem';
  key: string;
}
```

```ts
{
  type: 'multiSet';
  keyValuePairs: Array<[string, string]>;
}
```

```ts
{
  type: 'multiMerge';
  keyValuePairs: Array<[string, string]>;
}
```

```ts
{
  type: 'multiRemove';
  keys: Array<string>;
};
```

### Source

An object with these methods:

```js
{
  getAllKeys(): Stream<Array<string>>;
  getItem(key: string): Stream<string>;
  multiGet(keys: Array<string>): Stream<Array<[string, string]>>;
}
```

## License

Copyright (C) 2018 Andre 'Staltz' Medeiros, licensed under MIT license

